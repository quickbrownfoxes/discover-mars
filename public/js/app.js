var app = angular.module('discoverMars', ['ngMaterial', 'ngMessages', 'material.svgAssetsCache']);
app.config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('');
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}]);

app.controller('DashController', function($scope, $location, $http) {
    $scope.rover = $location.hash();
    $scope.camera = "fhaz";
    $scope.selectedSol;
    $scope.switchStatus = false;

    $scope.cameras = {
        'EDL_RUCAM': 0,
        'EDL_RDCAM': 0,
        'EDL_DDCAM': 0,
        'EDL_PUCAM1': 0,
        'EDL_PUCAM2': 0,
        'NAVCAM_LEFT': 0,
        'NAVCAM_RIGHT': 0,
        'MCZ_RIGHT': 0,
        'MCZ_LEFT': 0,
        'FRONT_HAZCAM_LEFT_A': 0,
        'FRONT_HAZCAM_RIGHT_A': 0,
        'REAR_HAZCAM_LEFT': 0,
        'REAR_HAZCAM_RIGHT': 0,
        'FHAZ': 0,
        'RHAZ': 0,
        'MAST': 0,
        'CHEMCAM': 0,
        'MAHLI': 0,
        'MARDI': 0,
        'NAVCAM': 0,
        'PANCAM': 0,
        'MINITIES': 0,
    };

    //var url = "https://api.nasa.gov/mars-photos/api/v1/rovers/" + $scope.rover + "/photos?sol=1000&camera=" + $scope.camera + "&api_key=bCnoqmWohjoici3G3k3nRKGnAMl0yy8gNEzFUA94";
    var url = "";
    var url1 = "https://api.nasa.gov/mars-photos/api/v1/manifests/" + $scope.rover + "?api_key=bCnoqmWohjoici3G3k3nRKGnAMl0yy8gNEzFUA94";

    $scope.getPhotos = function() {
        if(!$scope.loading) {
            $scope.loading = true;
        }
        if ($scope.switchStatus) {
            if ($scope.selectedSol < 1) {
                $scope.selectedSol = 1;
            }
            else if ($scope.selectedSol > $scope.maxSol) {
                $scope.selectedSol = $scope.maxSol;
            } else if (isNaN($scope.selectedSol)) {
                $scope.selectedSol = 1;
            }
            url = "https://api.nasa.gov/mars-photos/api/v1/rovers/" + $scope.rover + "/photos?sol=" + $scope.selectedSol + "&api_key=bCnoqmWohjoici3G3k3nRKGnAMl0yy8gNEzFUA94";
        }
        else {
            url = "https://api.nasa.gov/mars-photos/api/v1/rovers/" + $scope.rover + "/photos?earth_date=" + $scope.myDate.getFullYear() + "-" + ($scope.myDate.getMonth() + 1) + "-" + $scope.myDate.getDate() + "&api_key=bCnoqmWohjoici3G3k3nRKGnAMl0yy8gNEzFUA94";
        }
        console.log("getPhotos(url): " + url);
        $http({
            method: 'GET',
            url: url,
            dataType: "json",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).then(function successCallback(response2) {
            console.log(response2.data.photos);
            $scope.photos = response2.data.photos;
            $scope.cameras = {
                'EDL_RUCAM': 0,
                'EDL_RDCAM': 0,
                'EDL_DDCAM': 0,
                'EDL_PUCAM1': 0,
                'EDL_PUCAM2': 0,
                'NAVCAM_LEFT': 0,
                'NAVCAM_RIGHT': 0,
                'MCZ_RIGHT': 0,
                'MCZ_LEFT': 0,
                'FRONT_HAZCAM_LEFT_A': 0,
                'FRONT_HAZCAM_RIGHT_A': 0,
                'REAR_HAZCAM_LEFT': 0,
                'REAR_HAZCAM_RIGHT': 0,
                'FHAZ': 0,
                'RHAZ': 0,
                'MAST': 0,
                'CHEMCAM': 0,
                'MAHLI': 0,
                'MARDI': 0,
                'NAVCAM': 0,
                'PANCAM': 0,
                'MINITIES': 0,
            };
            for (var i = 0; i < $scope.photos.length; i++) {
                $scope.cameras[$scope.photos[i].camera.name]++;
            };

            console.log($scope.cameras);
            if ($scope.cameras['FHAZ'] > 0) {
                $scope.activeCamera = 'FHAZ';
            }
            else if ($scope.cameras['RHAZ'] > 0) {
                $scope.activeCamera = 'RHAZ';
            }
            else if ($scope.cameras['MAST'] > 0) {
                $scope.activeCamera = 'MAST';
            }
            else if ($scope.cameras['CHEMCAM'] > 0) {
                $scope.activeCamera = 'CHEMCAM';
            }
            else if ($scope.cameras['MAHLI'] > 0) {
                $scope.activeCamera = 'MAHLI';
            }
            else if ($scope.cameras['MARDI'] > 0) {
                $scope.activeCamera = 'MARDI';
            }
            else if ($scope.cameras['NAVCAM'] > 0) {
                $scope.activeCamera = 'NAVCAM';
            }
            else if ($scope.cameras['PANCAM'] > 0) {
                $scope.activeCamera = 'PANCAM';
            }
            else if ($scope.cameras['MINITIES'] > 0) {
                $scope.activeCamera = 'MINITIES';
            }
            else if ($scope.cameras['EDL_RUCAM'] > 0) {
                $scope.activeCamera = 'EDL_RUCAM';
            }
            else if ($scope.cameras['EDL_RDCAM'] > 0) {
                $scope.activeCamera = 'EDL_RDCAM';
            }
            else if ($scope.cameras['EDL_PUCAM1'] > 0) {
                $scope.activeCamera = 'EDL_PUCAM1';
            }
            else if ($scope.cameras['EDL_PUCAM2'] > 0) {
                $scope.activeCamera = 'EDL_PUCAM2';
            }
            else if ($scope.cameras['NAVCAM_LEFT'] > 0) {
                $scope.activeCamera = 'NAVCAM_LEFT';
            }
            else if ($scope.cameras['NAVCAM_RIGHT'] > 0) {
                $scope.activeCamera = 'NAVCAM_RIGHT';
            }
            else if ($scope.cameras['MCZ_RIGHT'] > 0) {
                $scope.activeCamera = 'MCZ_RIGHT';
            }
            else if ($scope.cameras['MCZ_LEFT'] > 0) {
                $scope.activeCamera = 'MCZ_LEFT';
            }
            else if ($scope.cameras['FRONT_HAZCAM_LEFT_A'] > 0) {
                $scope.activeCamera = 'FRONT_HAZCAM_LEFT_A';
            }
            else if ($scope.cameras['FRONT_HAZCAM_RIGHT_A'] > 0) {
                $scope.activeCamera = 'FRONT_HAZCAM_RIGHT_A';
            }
            else if ($scope.cameras['REAR_HAZCAM_LEFT'] > 0) {
                $scope.activeCamera = 'REAR_HAZCAM_LEFT';
            }
            else if ($scope.cameras['REAR_HAZCAM_RIGHT'] > 0) {
                $scope.activeCamera = 'REAR_HAZCAM_RIGHT';
            }

            console.log($scope.activeCamera);
            $scope.loading = false;
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    var getManifest = function() {
        $scope.loading = true;
        $http({
            method: 'GET',
            url: url1,
            dataType: "json",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        }).then(function successCallback(response) {
            console.log(response.data.photo_manifest);
            var date = response.data.photo_manifest.landing_date.split("-");
            var date2 = response.data.photo_manifest.max_date.split("-");
            $scope.minDate = new Date(
                date[0],
                date[1] - 1,
                date[2]);

            $scope.maxDate = new Date(
                date2[0],
                date2[1] - 1,
                date2[2]);

            $scope.myDate = $scope.maxDate;
            $scope.maxSol = response.data.photo_manifest.max_sol;
            $scope.selectedSol = response.data.photo_manifest.max_sol;
            $scope.getPhotos();
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });
    }

    getManifest();

    $scope.changeCamera = function(newCam) {
        $scope.activeCamera = newCam;
        console.log($scope.activeCamera);
    }
    
    $scope.expandImage = function(url) {
        $scope.clickedImage = url;
    }

});
